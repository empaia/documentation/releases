# Platform Releases

## Main Features

### v1

* AI apps are packaged as docker containers and are executed in the backend
* AI apps connect to the App API v0
* Workbench Client 1.0 is a generic web-based UI to run AI apps and visualize results

### v2

* AI apps are bundled with web-based App UIs to implement specific user interactions and visualizations
* App UIs connect to the Workbench v2 Scopes API
* Workbench Client 2.0 embeds App UIs as iframe

### v3

* AI apps may implement different execution modes, including whole slide preprocessing
* App UIs may generate reports
* Workbench Client 3.0 supports a Generic App UI for test and research purposes
* Support for coexisting API versions
* Improved data read-write performance of the platform reference implementation

### v3 - Pixelmaps

* Platform Release v3 extension described in [EMP-0102](https://gitlab.com/empaia/emps/-/blob/main/emp-0102.md)
* Support for new Pixelmaps data type
* Support for Rendering Hints in EAD

### v3 - Multi-User Data Sharing in App UI Scopes

* Platform Release v3 extension described in [EMP-0104](https://gitlab.com/empaia/emps/-/blob/main/emp-0104.md)
* Support for retrieval of data elements from other users' scopes via the Workbench v3 Scopes API

### v3 - Pixelmaps Tile Transfer Compression

* Platform Release v3 extension described in [EMP-0107](https://gitlab.com/empaia/emps/-/blob/main/emp-0107.md)
* Support for optional `gzip` compression when sending and receiving Pixelmap tiles

### v3 - Pixelmaps Level and Resolution Info

* Platform Release v3 extension described in [EMP-0108](https://gitlab.com/empaia/emps/-/blob/main/emp-0108.md)
* Support for a new Pixelmaps endpoint providing the level and resolution info from the referenced WSI

## Version Compatibility

| Platform Release | EAD | Workbench Client | Medical Data API | App API | Workbench API |
| --- | --- | --- | --- | --- | --- |
| v1 `deprecated` | v1-draft3 | v1 | v1 | v0 | v1 |
| v2 | v1-draft3 | v2 | v1 | v0 | v2 |
| v3 | v3 | v3 | v3 | v3 | v3 |

## Medical Data Storage

The Medical Data API allows for storage separation of read-write data between API versions. This decreases the implementation efforts for third-party clinical systems, that may provide multiple API versions at once. The following diagram shows the data separation according to the platform reference implementation.

<img src="images/medical_data_api_versions.svg"></img>

### Read-Only Data

The EMPAIA API specification considers cases and slides to be read-only data. The data is not separated by API version, because in a clinical system integration this data would be fetched from an APLIS, IMS, VNA or PACS system. The way data is ingested into such systems is entirely system specific and therefore not part of the EMPAIA API specification. The EMPAIA platform reference implementation (Clinical Data Service) provides private routes to ingest such data for test purposes and the database tables and WSI files are shared among multiple API versions.

### Read-Write Data

Data types like examinations, scopes, jobs, annotations, primitives, etc. are native to the EMPAIA platform and created during the diagnostic process. Therefore routes for reading and writing such data elements are part of the API specification. With each release changes to the specification might occur (e.g. adding and removing fields from data models). In order to avoid complicated forwards and backwards compatibility measures in the service code, the data can be separated by API version. The platform reference implementation (Examination Service, Job Service, Annotation Service) creates separate database tables to achieve such a separation. Third-party clinical systems conforming to the specification might follow a different implementaion approach.

## App Versioning

The core of an App is defined by its technical components:

* EAD
* Container Image
* App UI (optional, if EMPAIA Generic App UI is used in a development or research context)
* Configurations (optional)

The App Version is defined by its namespace as specified in the App's EAD. See the following namespace example:

```
org.empaia.vendor_name.app_name.v3.0
```

From EAD `v3` onwards this is specified as the Platform release version followed by an incremental number (e.g. `v3.0`, `v3.1` where `3` is the Platform release version).

Legacy Apps that use EAD `v1-draft3` only use a single incremental number to define the App version (e.g. `v1`, `v2`, `v3`).

### Publishing a New App

App developers can submit their App to the EMPAIA Team to receive feedback or to get the App listed in the [EMPAIA Portal](https://portal.empaia.org/). For a new organization (company or research institute) that submits an app for the first time the `vendor_name` part of the namespace must be chosen. For each new App the `app_name` part of the namespace must be chosen as well. The technical components (EAD, Container Image, App UI, Configurations) of the App form a unit and must be tested together using the EMPAIA App Test Suite (EATS) before submission.

For further information about the technical components, please refer to the [App Developer Docs](https://developer.empaia.org/app_developer_docs/v3/#/publish).

Please also provide anonymized WSI files and EATS input files for testing when submitting an App to the EMPAIA Team.

### Publishing an App Update

App developers can submit a new version of an existing App. In this case, the `vendor_name` and `app_name` do not change and the App version is incremented by 1, e.g. from `v3.0` to `v3.1`. This results in a namespace change and also affects the class values (see next section). Therefore App UI and Docker Container must be updated to match the new namespace. Again the technical components of the updated App must be tested together using the EATS before submission.

The data produced by a specific App version is always directly associated with that specific App version, ensuring that the data cannot be mixed with other versions of the same App. Therefore it is not required to maintain backwards or forwards compatibility between App versions. Each App version is registered in the platform with a distinct App ID, such that it can be distinguished from other Apps and other versions of the same App at any time.

#### Namespaces in Class Values

Since the namespace is used as part of class values, the software code in the Container Image and App UI must handle the specific class values depending on the App version. See the following class value example:

```
org.empaia.vendor_name.app_name.v3.0.classes.tumor
org.empaia.vendor_name.app_name.v3.0.classes.non_tumor
```

The coupling of class values to the namespace is necessary, because App updates might change the semantics of classes. See the following class value example:

```
org.empaia.vendor_name.app_name.v3.1.classes.tumor.positive
org.empaia.vendor_name.app_name.v3.1.classes.tumor.negative
org.empaia.vendor_name.app_name.v3.1.classes.non_tumor
```

Compared to the two available class values of App version `v3.0`, the new App version `v3.1` distiguishes three class values.

### Publishing an App Hotfix

In certain situations it might be necessary to submit a hotfix for a technical componant of an existing App version. The EMPAIA Team is able to change all App components except for the EAD for a certain App version inplace. Hotfixes **are not recommended** and must be well justified. Please submit a new App version instead of a hotfix if possible. A hotfix will not alter the namespace or the App version.

### App View

In order to list an App in the [EMPAIA Portal](https://portal.empaia.org/) additional text descriptions, tags and images are required. Please refer to one of the Apps that are already listed in the Portal as an example. The following resources are required to best represent an App on its own Portal page.

* Before/after screenshots for top of App page (wide format for before/after slider)
* English description text for App
* Tissue, stain, indication, analysis based on the following list of possible tags: https://gitlab.com/empaia/integration/definitions/-/blob/main/tags/tags.csv
* Workflow screenshots for bottom of App page
* English description text for each workflow screenshot
* Company logo (square or circle)

### Portal App

A Portal App is a meta data structure for Apps and their respective versions. An example is shown below:

* Portal App X
  * Platform v1 (EAD v1-draft3, Workbench Client v1) `deprecated`
    * App v1
      * active: false
    * App v2
      * active: false
    * App v3
      * active: **true**
  * Platform v2 (EAD v1-draft3, Workbench Client v2)
    * App v1
      * active: false
    * App v2
      * active: false
    * App v3
      * active: **true**
  * Platform v3 (EAD v3, Workbench Client v3)
    * App v3.0 
      * active: false
    * App v3.1
      * active: **true**
* Portal App Y
  * Platform v1
    * ...
  * Platform v2
    * ...
  * Platform v3
    * ...

  Each Portal App may support multiple Platform versions, such that it can be used in multiple Workbench Client versions. Apps that can be used with Platform versions `v1` and `v2` use the legacy versioning scheme. From Platform version `v3` onwards, the new versioning scheme is used, that includes the Platform version as the first part of the App version.

  For each Platform version a single app with the attribute `active: true` can be defined. Such an active app will be selected for new Examinations in the respective Workbench Client and to display App details in the EMPAIA Portal. Other App versions can still be accessed in the Workbench Client through already existing Examinations. Usually, the latest version is active, but exceptions may apply (e.g. to perform a rollback).
